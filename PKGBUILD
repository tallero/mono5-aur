# Maintainer: Felix Golatofski
# Maintainer: Mufatti Alex <me@alexmufatti.it>
# Contributor: Pellegrino Prevete <pellegrinoprevete@gmail.com>
# Contributor: monoluser

pkgname=mono5
_pkgname=mono
_gitcommit=feca91f7eb1b440a6520cf754370b98271339d1a
pkgver=5.20.1.34
pkgrel=3
pkgdesc="Free implementation of the .NET platform including runtime and compiler"
url='http://www.mono-project.com/'
arch=('x86_64'
      'i686')
license=('GPL' 'LGPL2.1' 'MPL')
depends=('zlib' 'libgdiplus>=4.2' 'sh' 'python' 'ca-certificates')
makedepends=('cmake' 'mono')
provides=("mono=${pkgver}" 'monodoc=${pkgver}')
conflicts=('mono' 'monodoc')
install=${_pkgname}.install
_url="https://github.com/${_pkgname}"
_submodules=("api-doc-tools"
             "api-snapshot"
	     "aspnetwebstack"
             "bockbuild"
             "boringssl"
             "cecil"
             "corert"
             "corefx"
             "ikdasm"
             "linker"
             "roslyn-binaries"
             "rx"
             "xunit-binaries")
_submodules_urls=($(for _module in "${_submodules[@]}"; do echo "git+${_url}/${_module}"; done))
source=(${_pkgname}::"git+${_url}/mono#commit=${_gitcommit}"
                     "git+${_url}/Newtonsoft.Json"
                     ${_submodules_urls[*]}
                     "git+${_url}/ikvm-fork"
                     "git+${_url}/reference-assemblies"
                     "git+${_url}/NUnitLite"
                     "git+${_url}/NuGet.BuildTasks"
                     "mono.binfmt.d")
sha256sums=('SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            '9a657fc153ef4ce23bf5fc369a26bf4a124e9304bde3744d04c583c54ca47425')


pkgver() {
  cd "${srcdir}"/${_pkgname}
  git describe --always --tags | sed 's/^v//;s/^mono-//;s/\([^-]*-g\)/r\1/;s/-/./g'
}

_fix_link_expr="s#git://github.com#https://github.com#g"

_fix_git_config() {
  local _cfg="${1}"
  sed -i "${_fix_link_expr}" "${_cfg}"
}

_fix_repo_links() {
  local _repo="${1}"
  _fix_git_config "${_repo}/.git/config"
  _fix_git_config "${_repo}/.gitmodules"
}

_fix_submodule_links() {
  local _repo="${1}"
  local _module="${2}"
  _fix_git_config "${_repo}/.git/modules/${_module}/config"
}

_fix_aot_makefile() {
  echo "ciao"
  # sed -i "s#profile_file:=#profile_file:= #g" "mcs/class/aot-compiler/Makefile"
  # sed -i "s#profile_arg:=#profile_arg:= #g" "mcs/class/aot-compiler/Makefile"
}

_fix_submodule() {
  local _repo="${1}"
  local _module="${2}"
  git submodule update --init "${_module}"
  _fix_submodule_links "${_repo}" "${_module}"
}

_fix_repo() {
  local _repo="${1}"
  local _module _pwd _submodules_dir=()
  _pwd="$(pwd)"
  _submodules_dir=($(for _module in "${_submodules[@]}"; do echo "external/${_module}"; done))
  _fix_repo_links "${_repo}"
  cd "${_repo}"
  _fix_aot_makefile
  for _module in "${_submodules_dir[@]}"; do
    _fix_submodule "${_repo}" "${_module}"
  done
  _fix_git_config "external/linker/.gitmodules"
  _fix_submodule "${_repo}" "external/linker"
  cd "${_pwd}"
}

prepare() {
  local _repo="${srcdir}/${_pkgname}"
  _fix_repo "${_repo}"
}

build() {
  cd "${srcdir}"/${_pkgname}

  local _opts=(--prefix='/usr'
               --sysconfdir='/etc'
               --bindir='/usr/bin'
               --sbindir='/usr/bin'
               --enable-minimal='aot'
               --with-mcs-docs='no')
  # build mono
  ./autogen.sh ${_opts[@]}
  make

  # build jay
  make -C mcs/jay
}

package() {
  cd "${srcdir}"/${_pkgname}
  make DESTDIR="${pkgdir}" install

  make -C mcs/jay DESTDIR="${pkgdir}" prefix=/usr INSTALL=../../install-sh install
  install -Dm 644 "${srcdir}/mono.binfmt.d" "${pkgdir}/usr/lib/binfmt.d/mono.conf"

  #fix .pc file to be able to request mono on what it depends, fixes #go-oo build
  sed -i -e "s:#Requires:Requires:" "${pkgdir}"/usr/lib/pkgconfig/mono.pc
  sed -i -e "s:/2.0/:/4.5/:g" "${pkgdir}"/usr/lib/pkgconfig/mono-nunit.pc
}

